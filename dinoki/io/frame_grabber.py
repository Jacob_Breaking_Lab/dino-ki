import numpy as np
from PIL import ImageGrab




class FrameGrabber(object):

    def __init__(self):
        raise NotImplementedError

    def get_frame(self):
        Bildschirm =  np.array(ImageGrab.grab(bbox=(0,0,800,640))) #Bild einladen; Ersten beiden Werte sind x,y Wert von der linken oberen Ecke aus; Format 800x640
        return Bildschirm 